﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticateDemo.Models.DbModels
{
    public class User
    {
        //Primary key that will be used in DB
        //You can add other properties in here as well
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }

    }
}

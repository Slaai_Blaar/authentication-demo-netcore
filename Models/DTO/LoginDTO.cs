﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticateDemo.Models.DTO
{
    public class LoginDTO
    {
        public String UserName  { get; set; }
        public  string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticateDemo.Models.DTO
{
    public class UserDTO
    {
        public String CompanyName { get; set; }
        public string Password { get; set; } //You might need to encrypt these
        public string UserName { get; set; }
        public bool IsActive { get; set; } = true; //Just a default
    }
}

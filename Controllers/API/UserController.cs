﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthenticateDemo.DAL.Interfaces;
using AuthenticateDemo.Models;
using AuthenticateDemo.Models.DTO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AuthenticateDemo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly IOptions<JwtAuthentication> _jwtAuthentication;

        public UserController(IUserRepository repository, IOptions<JwtAuthentication> jwtAuthentication)
        {
            _jwtAuthentication = jwtAuthentication ?? throw new ArgumentNullException(nameof(jwtAuthentication));
            _repository = repository;
        }

        [HttpPost, Route("CreateUser")]
        public IActionResult  CreateUser(UserDTO dto)
        {
            _repository.RegisterUser(dto);
            return Ok();

        }

        [HttpPost]
        [AllowAnonymous]
        [Route("GenerateToken")]
        public IActionResult GenerateToken([FromBody]GenerateTokenModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(model);

            // TODO use your actual logic to validate a user
            if (!_repository.UserAuthenticated(model.Username, model.Password))
                return Unauthorized();

            var token = new JwtSecurityToken(
                issuer: _jwtAuthentication.Value.ValidIssuer,
                audience: _jwtAuthentication.Value.ValidAudience,
                claims: new[]
                {
                    // You can add more claims if you want
                    new Claim(JwtRegisteredClaimNames.Sub, model.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                },
                expires: DateTime.UtcNow.AddDays(30),
                notBefore: DateTime.UtcNow,
                signingCredentials: _jwtAuthentication.Value.SigningCredentials);

            return Ok(new
            {
                //  var test = new JwtSecurityTokenHandler().
                token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }

        /**
         *
         * You need to have this attribute to enable the security to be active
         * You can add it to classes/methods. 
         */
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme), HttpGet, Route("test")]
        public IActionResult Get()
        {
            return Ok("This is our return.");
        }

        public class GenerateTokenModel
        {
            [Required]
            public string Username { get; set; }
            [Required]
            public string Password { get; set; }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticateDemo.Models.DbModels;
using Microsoft.EntityFrameworkCore;

namespace AuthenticateDemo.DAL
{
    public class DataContext :DbContext
    {
        //We will Use Code first for the database access layer and table creations
        public DataContext(DbContextOptions dbOptions) : base(dbOptions)
        {

        }
        //These users will be the company access details, username and password still can come in here
        //You can also activate or de-active the users in here, it up to you!
        public DbSet<User> Users { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticateDemo.DAL.Interfaces;
using AuthenticateDemo.Models.DbModels;
using AuthenticateDemo.Models.DTO;

namespace AuthenticateDemo.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context)
        {
            _context = context;
        }

        //You can change the query when you want and need
        public bool UserAuthenticated(string username, string password)
        {
            return _context.Users.Where(x => x.UserName == username && x.Password == password).Any();

        }

        public void RegisterUser(string companyName, string userName, string password)
        {
            RegisterUser(new UserDTO()
            {
                Password = password,
                UserName = userName,
                CompanyName = companyName
            });
        }

        public async void RegisterUser(UserDTO userDto)
        {
            //You might need to implement Auto mapper here,
            //I'm a little tired now so you can read up on it ;)

            _context.Users.Add(new User()
            {
                Password =  userDto.Password,
                UserName = userDto.UserName,
                CompanyName = userDto.CompanyName
            });
            await _context.SaveChangesAsync();
        }
    }
}

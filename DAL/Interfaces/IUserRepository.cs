﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticateDemo.Models.DTO;

namespace AuthenticateDemo.DAL.Interfaces
{
    public interface IUserRepository
    {
        bool UserAuthenticated(string username, string password);

        void RegisterUser(string companyName, string userName, string password);

        void RegisterUser(UserDTO userDto);
    }
}
